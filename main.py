import uvicorn

from service.app import init_app

app = init_app()
if __name__ == "__main__":
    uvicorn.run(app, port=7777, host="0.0.0.0")
