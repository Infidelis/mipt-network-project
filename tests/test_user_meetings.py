from sqlmodel import Session
from starlette.testclient import TestClient

from service.models import MeetingCreate, MeetingUpdateStatus
from service.types import RequestStatus
from tests.utils import create_meeting, get_user_meetings


def test_get_users_for_meeting(
    meeting_create: MeetingCreate,
    client: TestClient,
    session: Session,
):
    user_id = meeting_create.users[0].id
    _ = create_meeting(meeting_create, client)

    meetings = get_user_meetings(user_id, client)
    assert meetings == [{"id": 1, "request_status": "pending"}]


def test_accept_meeting(
    meeting_create: MeetingCreate, session: Session, client: TestClient
):
    user_id = meeting_create.users[0].id
    meeting_id = create_meeting(meeting_create, client)

    data = MeetingUpdateStatus(request_status=RequestStatus.APPROVED)
    resp = client.patch(f"/user/meetings/{meeting_id}", data=data.json())
    assert resp.status_code == 200

    meetings = get_user_meetings(user_id, client)
    assert meetings == [{"id": 1, "request_status": RequestStatus.APPROVED}]
