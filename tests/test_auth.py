import time
from datetime import timedelta

import pytest
from fastapi import HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlmodel import Session
from starlette.testclient import TestClient

from service.models import UserBase, UserCreate
from service.routers.auth import create_access_token, get_user_orm
from tests.utils import create_user, set_token


def test_create_user(user_base: UserBase, client: TestClient):
    _ = create_user(user_base, client)


def test_auth_not_exists_user(user_base: UserCreate, client: TestClient):
    resp = client.post(
        "/token",
        data={
            "username": user_base.email,
            "password": "password",
            "grant_type": "password",
        },
        headers={"content-type": "application/x-www-form-urlencoded"},
    )
    assert resp.status_code == 404


def test_auth_exists_user(user_base: UserBase, client: TestClient):
    email, password = create_user(user_base, client)
    set_token(email, password, client)


def test_auth_failed_login_user(user_base: UserBase, client: TestClient):
    email, password = create_user(user_base, client)
    resp = client.post(
        "/token",
        data={"username": email, "password": password + "1", "grant_type": "password"},
        headers={"content-type": "application/x-www-form-urlencoded"},
    )
    assert resp.status_code == 401


def test_auth_token_expires(user_base: UserBase, client: TestClient):
    email, password = create_user(user_base, client)
    set_token(email, password, client, expires_in=1)
    time.sleep(1)
    set_token(email, password, client, expires_in=1)


def test_auth_token_invalid(user_base: UserBase, client: TestClient):
    email, password = create_user(user_base, client)
    client.headers["Authorization"] = f"Bearer random_token"

    resp = client.get("/user/")
    assert resp.status_code == 401


def test_auth_token_invalid_create(
    user_base: UserBase, client: TestClient, session: Session
):
    access_token = create_access_token({}, timedelta(seconds=120))
    with pytest.raises(HTTPException):
        get_user_orm(access_token, session)


def test_auth_token_get_unregistred_user(
    user_base: UserBase, client: TestClient, session: Session
):
    access_token = create_access_token(
        {"sub": "no_registered@user.com"}, timedelta(seconds=120)
    )
    with pytest.raises(HTTPException):
        get_user_orm(access_token, session)
