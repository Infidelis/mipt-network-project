from typing import Optional

from starlette.testclient import TestClient

from service.models import MeetingCreate, UserBase, UserCreate
from service.types import Email


def create_meeting(meeting_create: MeetingCreate, client):
    resp = client.post("/meeting/", data=meeting_create.json())
    assert resp.status_code == 200
    j_resp = resp.json()
    meeting_id = j_resp["meeting_id"]

    return meeting_id


def create_user(
    user_base: UserBase, client: TestClient, password: str = "password"
) -> tuple[Email, str]:
    user_base = UserCreate(**user_base.dict(), password=password)
    resp = client.post("/user/", data=user_base.json())
    assert resp.status_code == 200
    return user_base.email, password


def set_token(
    email: Email, password: str, client: TestClient, expires_in: Optional[int] = None
) -> None:
    scope = ""
    if expires_in:
        scope += f"expires:{expires_in} "
    resp = client.post(
        "/token",
        data={
            "username": email,
            "password": password,
            "grant_type": "password",
            "scope": scope,
        },
        headers={"content-type": "application/x-www-form-urlencoded"},
    )
    assert resp.status_code == 200
    j_resp = resp.json()
    assert j_resp["token_type"] == "bearer"
    client.headers["Authorization"] = f"Bearer {j_resp['access_token']}"


def get_user_meetings(user_id: int, client: TestClient):
    resp = client.get(
        f"/user/{user_id}/meetings",
        params={
            "offset": 0,
            "limit": 10,
        },
    )
    j_resp = resp.json()
    assert j_resp["id"] == user_id
    return j_resp["meetings"]


def create_and_set_token(user_base: UserBase, client: TestClient):
    email, password = create_user(user_base, client)
    set_token(email, password, client)
