import json

import pytest
from sqlmodel import Session
from starlette.testclient import TestClient

from service.models import UserBase, UserUpdate
from tests.utils import create_and_set_token


def test_create_invalid_email():
    with pytest.raises(ValueError):
        UserBase(
            first_name="Valya",
            second_name="Yurievich",
            last_name="Mamedov",
            email="v.mamedow@",
        )


def test_delete_user(user_base: UserBase, session: Session, client: TestClient):
    create_and_set_token(user_base, client)
    resp = client.delete(f"/user/")
    assert resp.status_code == 200
    json_r = resp.json()
    assert json_r["ok"] == True


def test_404_delete_user(client: TestClient):
    resp = client.delete(f"/user/")
    assert resp.status_code == 401


def test_get_user(user_base: UserBase, session: Session, client: TestClient):
    create_and_set_token(user_base, client)
    resp = client.get(f"/user/")
    assert resp.status_code == 200
    json_r = resp.json()
    assert json_r.pop("id") == 1
    assert json_r.pop("created_at_dttm")
    assert json_r.pop("updated_at_dttm")
    assert json_r == json.loads(user_base.json())


def test_get_user_id_by_email(user_base: UserBase, client: TestClient):
    create_and_set_token(user_base, client)
    resp = client.get(f"/user/{user_base.email}")
    assert resp.status_code == 200
    j_resp = resp.json()
    assert j_resp["id"] == 1


def test_404_get_user_id_by_email(user_base: UserBase, client: TestClient):
    create_and_set_token(user_base, client)
    resp = client.get(f"/user/v@tinkoff.ru")
    assert resp.status_code == 404


def test_user_update(user_base: UserBase, session: Session, client: TestClient):
    create_and_set_token(user_base, client)
    new_user_base = UserUpdate(
        first_name=user_base.first_name + "10", email=user_base.email + "e"
    )
    resp = client.patch(
        f"/user/",
        data=new_user_base.json(exclude_none=True, exclude_unset=True),
    )
    assert resp.status_code == 200
    json_r = resp.json()
    assert json_r["first_name"] == new_user_base.first_name
    assert json_r["email"] == new_user_base.email


def test_401_user_update(user_base: UserBase, client: TestClient):
    resp = client.patch(f"/user/", data=user_base.json())
    assert resp.status_code == 401
