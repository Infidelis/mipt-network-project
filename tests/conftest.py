from datetime import datetime

import pytest
from fastapi.testclient import TestClient
from sqlmodel import create_engine, Session, SQLModel
from sqlmodel.pool import StaticPool

from service.app import init_app
from service.database import get_session
from service.models import MeetingBase, MeetingCreate, UserBase, UserCreate
from tests.utils import create_user, set_token


@pytest.fixture(name="session")
def session_fixture() -> Session:
    engine = create_engine(
        "sqlite://", connect_args={"check_same_thread": False}, poolclass=StaticPool
    )
    SQLModel.metadata.create_all(engine)
    with Session(engine) as session:
        yield session


@pytest.fixture(name="client")
def client_fixture(session: Session):
    app = init_app(test=True)

    def get_session_override():  #
        return session

    app.dependency_overrides[get_session] = get_session_override  #

    with TestClient(app) as client_:
        yield client_


@pytest.fixture
def user_base() -> UserBase:
    return UserBase(
        first_name="Valya",
        second_name="Yurievich",
        last_name="Mamedov",
        email="v.mamedov@tinkoff.ru",
    )


@pytest.fixture
def meeting_base() -> MeetingBase:
    return MeetingBase(
        name="Встреча одноклассников",
        from_dttm=datetime.strptime("18/09/19 01:55:19", "%d/%m/%y %H:%M:%S"),
        till_dttm=datetime.strptime("18/09/20 01:55:19", "%d/%m/%y %H:%M:%S"),
        location=None,
        description=None,
    )


@pytest.fixture
def meeting_create(
    meeting_base: MeetingBase,
    user_base: UserBase,
    client: TestClient,
):
    email, password = create_user(user_base, client)
    set_token(email, password, client)

    meeting_create = MeetingCreate(
        users=[MeetingCreate.User(id=1)], **meeting_base.dict()
    )
    return meeting_create
