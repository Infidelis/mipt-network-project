import json

from fastapi.testclient import TestClient
from sqlmodel import Session

from service.models import (
    Meeting,
    MeetingCreate,
    MeetingUpdate,
    MeetingUpdateUsers,
    User,
    UserBase,
)
from service.types import Email
from tests.utils import create_meeting, create_user


def test_create_meeting(
    meeting_create: MeetingCreate,
    client: TestClient,
):
    meeting_id = create_meeting(meeting_create, client)
    assert meeting_id == 1


def test_404_create_meeting(
    meeting_create: MeetingCreate,
    client: TestClient,
):
    meeting_create.users.append({"id": 2})
    resp = client.post("/meeting/", data=meeting_create.json())
    assert resp.status_code == 400


def test_delete_meeting(
    meeting_create: MeetingCreate,
    client: TestClient,
    session: Session,
):
    meeting_id = create_meeting(meeting_create, client)

    resp = client.delete(f"/meeting/{meeting_id}")
    assert resp.status_code == 200
    assert session.get(Meeting, meeting_id) is None
    assert session.get(User, meeting_create.users[0].id) is not None


def test_404_delete_meeting(
    meeting_create: MeetingCreate,
    client: TestClient,
):
    resp = client.delete("/meeting/1")
    assert resp.status_code == 404


def test_meeting_patch(
    meeting_create: MeetingCreate,
    client: TestClient,
    session: Session,
):
    meeting_id = create_meeting(meeting_create, client)

    edit_meeting = MeetingUpdate(description="nothing")
    resp = client.patch(f"/meeting/{meeting_id}", data=edit_meeting.json())
    j_resp = resp.json()
    assert j_resp["description"] == "nothing"


def test_meeting_patch_users(
    meeting_create: MeetingCreate,
    user_base: UserBase,
    client: TestClient,
    session: Session,
):
    meeting_id = create_meeting(meeting_create, client)
    user_base.email = Email("v.mamedov2@tinkoff.ru")
    email, password = create_user(user_base, client)
    user_ = client.get(f"/user/{email}").json()

    d = MeetingUpdateUsers(
        users=[
            {"id": user_["id"]},
        ]
    )
    resp = client.patch(f"/meeting/{meeting_id}/users", data=d.json())
    assert resp.status_code == 200
    j_resp = resp.json()
    assert j_resp["users"] == [
        {"id": user_["id"], "request_status": "pending"},
    ]
    assert session.get(Meeting, meeting_id).users[0].id == user_["id"]


def test_meeting_get(
    meeting_create: MeetingCreate,
    client: TestClient,
    session: Session,
):
    meeting_id = create_meeting(meeting_create, client)

    resp = client.get(f"/meeting/{meeting_id}")
    assert resp.status_code == 200
    json_r = resp.json()
    assert json_r.pop("id") == meeting_id
    assert json_r.pop("created_at_dttm")
    assert json_r.pop("updated_at_dttm")

    d_meeting_create = json.loads(meeting_create.json())
    assert d_meeting_create.pop("users")
    assert json_r == d_meeting_create


def test_meeting_get_users(
    meeting_create: MeetingCreate,
    client: TestClient,
    session: Session,
):
    meeting_id = create_meeting(meeting_create, client)

    resp = client.get(f"/meeting/{meeting_id}/users")
    assert resp.status_code == 200
    json_r = resp.json()
    assert json_r.pop("id") == meeting_id
    assert [{"id": 1, "request_status": "pending"}] == json_r["users"]
