RUN := poetry run
SRC_DIR := service
TEST_DIR := tests
# Allow arguments to be passed in make
PARAMS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
# Turn them into do-nothing targets
$(eval $(PARAMS):;@:)

.PHONY: install
install:
	@echo "Installing dependencies" && poetry install

.PHONY: test
test:
	@echo "[ \033[00;33mRunning tests with coverage\033[0m ]" && \
 		$(RUN) pytest -s --cov-report xml --cov-report term --cov=$(SRC_DIR) $(PARAMS) $(TEST_DIR)

.PHONY: format
format:
	@echo "[ \033[00;33mRunning Black linter\033[0m ]" && $(RUN) black .

.PHONY: lint
lint:
	@#echo "[ \033[00;33mRunning MyPy linter\033[0m ]" && $(RUN) mypy .
	@echo "[ \033[00;33mChecking Black linter\033[0m ]" && $(RUN) black --check .


.PHONY: clean
clean:
	find . -name "*.pyc" -exec rm -f {} +
	find . -name "*.pyo" -exec rm -f {} +
	find . -name "*~" -exec rm -f {} +
	find . -name "__pycache__" -exec rm -fr {} +

docker-pg-up:
	@echo "Docker compose up pg" && docker-compose -f docker/docker-compose.pg.yml up

run-server:
	@echo "run" && $(RUN) uvicorn main:app --host 0.0.0.0 --port 7777


