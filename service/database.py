import time
from pathlib import Path

from pydantic import BaseSettings, Field
from sqlalchemy.exc import OperationalError
from sqlmodel import create_engine, Session, SQLModel


class PgSettings(BaseSettings):
    host: str = "localhost"
    user: str = "user"
    password: str = "pass"
    db: str = "database"
    port: str = "5432"

    class Config:
        env_file = Path.cwd() / ".env"
        env_prefix = "POSTGRES_"


pgs = PgSettings()

pgsql_url = f"postgresql://{pgs.user}:{pgs.password}@{pgs.host}:{pgs.port}/{pgs.db}"

# wait 5 sec
for _ in range(10):
    try:
        engine = create_engine(pgsql_url)
        break
    except OperationalError:
        time.sleep(0.5)


def create_db_and_tables() -> None:  # pragma: no cover
    """Создаем все необходимые базы данных"""
    SQLModel.metadata.create_all(engine)


def get_session() -> Session:  # pragma: no cover
    """Получаем сессию пользователя, FastAPI после закрывает соединение через Depends"""
    with Session(engine) as session:
        yield session
