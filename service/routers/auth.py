import os
from datetime import datetime, timedelta
from typing import Optional

from fastapi import APIRouter
from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import jwt, JWTError
from passlib.context import CryptContext
from pydantic import BaseModel
from sqlmodel import col, select, Session
from starlette import status
from starlette.requests import Request

from service.database import get_session
from service.models import IdRequired, Meeting, User
from service.types import Email

SECRET_KEY = os.getenv(
    "SECRET_KEY", "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
)
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

auth_router = APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None


def verify_password(plain_password: str, hashed_password: str):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password: str):
    return pwd_context.hash(password)


def get_user_orm_by_email(user_email: Email, session: Session) -> User:
    """Получаем orm пользователя по meeting_id"""
    statement = select(User).where(User.email == user_email)
    user = session.execute(statement).one_or_none()
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return user[0]


def authenticate_user(
    user_email: Email, password: str, session: Session
) -> Optional[User]:
    user = get_user_orm_by_email(user_email, session)
    if not verify_password(password, user.hashed_password):
        return
    return user


def create_access_token(data: dict, expires_delta: timedelta):
    to_encode = data.copy()
    expire = datetime.utcnow() + expires_delta
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def get_user_orm(
    token: str = Depends(oauth2_scheme), session: Session = Depends(get_session)
) -> User:
    """Получаем orm пользователя по meeting_id"""
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = get_user_orm_by_email(Email(token_data.username), session)
    return user


@auth_router.post("/token", response_model=Token)
async def login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(),
    session: Session = Depends(get_session),
):
    user = authenticate_user(Email(form_data.username), form_data.password, session)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    expires_in_seconds = ACCESS_TOKEN_EXPIRE_MINUTES * 60
    for scope in form_data.scopes:
        if scope.startswith("expires:"):  # pragma: no cover
            expires_in_seconds = int(scope.split(":")[1])
            break

    access_token_expires = timedelta(seconds=expires_in_seconds)
    access_token = create_access_token(
        data={"sub": user.email}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


def get_meeting_orm(
    meeting_id: int, session: Session = Depends(get_session)
) -> Meeting:
    """Получаем orm встречи по meeting_id"""
    db_meeting = session.get(Meeting, meeting_id)
    if not db_meeting:  # pragma: no cover
        raise HTTPException(status_code=404, detail="Meeting not found")
    return db_meeting


def get_user_orms_from_ids(
    users: list["IdRequired"], session: Session, _: User = Depends(get_user_orm)
):
    """Получаем orm объекты пользователей по user_id"""
    user_ids = [user.id for user in users]
    statement = select(User).filter(col(User.id).in_(user_ids))
    users_ = session.exec(statement).all()
    if len(set(user_ids)) != len(users_):
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Some users have invalid ids")
    return users_
