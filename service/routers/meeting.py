from fastapi import APIRouter, Depends
from sqlmodel import col, select, Session

from service.database import get_session
from service.models import (
    Meeting,
    MeetingCreate,
    MeetingReadWithoutUsers,
    MeetingReadWithUsers,
    MeetingUpdate,
    MeetingUpdateUsers,
    UserMeetingLink,
)
from service.routers.utils import OffsetAndLimitParams
from service.routers.auth import get_meeting_orm, get_user_orm, get_user_orms_from_ids

meeting_router = APIRouter(
    prefix="/meeting",
    dependencies=[Depends(get_user_orm)],
)


@meeting_router.post("/")
def create_meeting(
    *,
    session: Session = Depends(get_session),
    meeting_create: MeetingCreate,
):
    """Создаем встречу пользователей"""
    users = get_user_orms_from_ids(meeting_create.users, session)
    db_meeting = Meeting.from_orm(meeting_create)
    db_meeting.users = users
    session.commit()
    return {"meeting_id": db_meeting.id}


@meeting_router.get("/{meeting_id}", response_model=MeetingReadWithoutUsers)
def get_meeting(
    *,
    db_meeting=Depends(get_meeting_orm),
):
    """Получаем пользователя по id"""
    return db_meeting


@meeting_router.get("/{meeting_id}/users", response_model=MeetingReadWithUsers)
def get_meeting_with_users(
    *,
    session: Session = Depends(get_session),
    db_meeting=Depends(get_meeting_orm),
    params=Depends(OffsetAndLimitParams),
):
    """Получаем пользотелей встречи"""
    meeting_id = db_meeting.id
    meeting_users = (
        session.exec(
            select(
                col(UserMeetingLink.user_id).label("id"), UserMeetingLink.request_status
            )
            .where(UserMeetingLink.meeting_id == meeting_id)
            .order_by(col(UserMeetingLink.created_at_dttm).desc())
            .offset(params.offset)
            .limit(params.limit)
        )
        .mappings()
        .all()
    )
    return dict(id=meeting_id, users=meeting_users)


@meeting_router.delete("/{meeting_id}")
def delete_meeting(
    *,
    session: Session = Depends(get_session),
    db_meeting=Depends(get_meeting_orm),
):
    """Удаляем встречу пользователей"""
    db_meeting.users = []
    session.delete(db_meeting)
    session.commit()
    return 200


@meeting_router.patch("/{meeting_id}", response_model=MeetingReadWithoutUsers)
def update_meeting(
    *,
    session: Session = Depends(get_session),
    db_meeting=Depends(get_meeting_orm),
    new_meeting: MeetingUpdate,
):
    """Обновляем информацию о встрече. КРОМЕ пользователей. См. метод `/{meeting_id}/users`"""
    meeting_data = new_meeting.dict(exclude_none=True)
    for key, value in meeting_data.items():
        setattr(db_meeting, key, value)
    session.commit()
    session.refresh(db_meeting)
    return db_meeting


@meeting_router.patch("/{meeting_id}/users", response_model=MeetingReadWithUsers)
def update_meeting_users(
    *,
    session: Session = Depends(get_session),
    db_meeting=Depends(get_meeting_orm),
    update_: MeetingUpdateUsers,
):
    """Обновляем пользователей встречи"""
    users = get_user_orms_from_ids(update_.users, session)
    db_meeting.users = users
    session.commit()
    return dict(id=db_meeting.id, users=users)
