import sqlmodel
from fastapi import APIRouter, Depends, HTTPException
from sqlmodel import col, select, Session, and_

from service.database import get_session
from service.models import (
    Meeting,
    MeetingUpdateStatus,
    User,
    UserCreate,
    UserMeetingLink,
    UserReadWithMeetings,
    UserReadWithoutMeetings,
    UserUpdate,
)
from service.routers.utils import (
    OffsetAndLimitParams,
    TimeFromTimeToParams,
)
from service.routers.auth import get_password_hash, get_user_orm, get_user_orm_by_email
from service.types import Email

user_router = APIRouter(prefix="/user")

user_router_with_auth = APIRouter(
    prefix="/user",
    dependencies=[Depends(get_user_orm)],
)


@user_router.post("/")
async def create_user(*, session: Session = Depends(get_session), user: UserCreate):
    """Создаем пользователя"""
    hashed_password = get_password_hash(user.password)
    db_user = User.from_orm(user, {"hashed_password": hashed_password})
    session.add(db_user)
    session.commit()
    return {"user_id": db_user.id}


@user_router_with_auth.delete("/")
def delete_user(
    *,
    session: Session = Depends(get_session),
    db_user=Depends(get_user_orm),
):
    """Удаляем пользователя"""
    session.delete(db_user)
    session.commit()
    return {"ok": True}


@user_router_with_auth.get("/", response_model=UserReadWithoutMeetings)
def get_user_by_id(
    *,
    db_user=Depends(get_user_orm),
):
    """Получаем пользователя по user_id"""
    return db_user


@user_router_with_auth.get("/{user_email}", response_model=UserReadWithoutMeetings)
def get_user_by_email(*, user_email: Email, session: Session = Depends(get_session)):
    """Получаем пользователя по его email"""
    return get_user_orm_by_email(user_email, session)


@user_router_with_auth.patch("/", response_model=UserReadWithoutMeetings)
def update_user(
    *,
    session: Session = Depends(get_session),
    db_user=Depends(get_user_orm),
    new_user: UserUpdate,
):
    """Обновляем информацию пользователя"""
    user_data = new_user.dict(exclude_none=True)
    for key, value in user_data.items():
        setattr(db_user, key, value)
    session.add(db_user)
    session.commit()
    session.refresh(db_user)
    return db_user


@user_router_with_auth.get("/{user_id}/meetings/", response_model=UserReadWithMeetings)
def get_user_meetings(
    *,
    session: Session = Depends(get_session),
    db_user=Depends(get_user_orm),
    params=Depends(OffsetAndLimitParams),
    time_params=Depends(TimeFromTimeToParams),
):
    """
    Получаем все встречи пользователя. Можно указать период времени, а также количество встреч в ответе
    (Отсортировано в порядке устаревания).
    """
    user_id = db_user.id
    user_meetings = (
        session.exec(
            select(
                col(UserMeetingLink.meeting_id).label("id"),
                UserMeetingLink.request_status,
            )
            .select_from(UserMeetingLink)
            .join(Meeting)
            .where(
                and_(
                    UserMeetingLink.user_id == user_id,
                    Meeting.created_at_dttm > time_params.time_from,
                    Meeting.created_at_dttm < time_params.time_to,
                )
            )
            .order_by(col(Meeting.created_at_dttm).desc())
            .offset(params.offset)
            .limit(params.limit)
        )
        .mappings()
        .all()
    )
    return dict(id=user_id, meetings=user_meetings)


@user_router_with_auth.patch("/meetings/{meeting_id}")
def user_new_meeting_status(
    *,
    session: Session = Depends(get_session),
    user: User = Depends(get_user_orm),
    meeting_id: int,
    new_status: MeetingUpdateStatus,
):
    """
    Обновляем статус встречи для пользователя
    """
    user_meeting = session.get(UserMeetingLink, (user.id, meeting_id))
    user_meeting.request_status = new_status.request_status
    session.commit()
    return 200
