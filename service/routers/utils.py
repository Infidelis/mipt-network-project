from datetime import datetime

from pydantic import BaseModel


class OffsetAndLimitParams(BaseModel):
    """Настраиваем выдачу встреч"""

    offset: int = 0
    limit: int = 5


class TimeFromTimeToParams(BaseModel):
    """Настраиваем период встреч"""

    time_from: datetime = datetime.min
    time_to: datetime = datetime.max
