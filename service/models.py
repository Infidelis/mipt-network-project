from datetime import datetime
from typing import List, Optional

from pydantic import conlist
from sqlalchemy import DateTime, UniqueConstraint
from sqlmodel import Field, Relationship, SQLModel

from service.types import Email, RequestStatus


#############
# Base MODEL types
#############
class CreatedAt(SQLModel):
    """Модель полей с временем создания и обновления записи"""

    created_at_dttm: datetime = Field(
        default=datetime.utcnow(), nullable=False, description="Время создания записи"
    )
    updated_at_dttm: datetime = Field(
        default_factory=datetime.utcnow,
        nullable=False,
        description="Время обновления записи",
    )


class MeetingUserStatus(SQLModel):
    request_status: RequestStatus = Field(
        default=RequestStatus.PENDING, description="Статус приглашения на встречу"
    )


class CreatedAtRequired(CreatedAt):
    created_at_dttm: datetime


class IdRequired(SQLModel):
    id: int


class MeetingUserStatusWithId(MeetingUserStatus, IdRequired):
    pass


#############

#############
# Base TABLE types
#############
class UserBase(SQLModel):
    __table_args__ = (UniqueConstraint("email"),)
    first_name: str = Field(description="имя")
    second_name: Optional[str] = Field(nullable=True, description="отчество")
    last_name: str = Field(description="фамилия")
    email: Email = Field(description="email, валидируется", index=True)


class MeetingBase(SQLModel):
    name: str = Field(description="название встречи", max_length=512)
    from_dttm: datetime = Field(description="дата начала встречи")
    till_dttm: datetime = Field(description="дата конца встречи")
    location: Optional[str] = Field(
        description="место встречи", nullable=True, max_length=512
    )
    description: Optional[str] = Field(
        description="описание", nullable=True, max_length=1024
    )


#############


#############
# TABLE types
#############
class UserMeetingLink(MeetingUserStatus, CreatedAt, table=True):
    user_id: Optional[int] = Field(
        default=None, foreign_key="user.id", primary_key=True
    )
    meeting_id: Optional[int] = Field(
        default=None, foreign_key="meeting.id", primary_key=True
    )

    user: "User" = Relationship()
    meeting: "Meeting" = Relationship()


class User(UserBase, CreatedAt, table=True):
    id: Optional[int] = Field(
        default=None, primary_key=True, description="Id пользователя"
    )
    meetings: List["Meeting"] = Relationship(
        back_populates="users", link_model=UserMeetingLink
    )
    hashed_password: str = Field()


class Meeting(MeetingBase, CreatedAt, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    users: List["User"] = Relationship(
        back_populates="meetings", link_model=UserMeetingLink
    )


#############


#############
# API model types
#############

#######
# Create
#######


class UserCreate(UserBase):
    password: str


class MeetingCreateUsers(SQLModel):
    class User(IdRequired):
        pass

    users: conlist(User, min_items=1, max_items=12) = []


class MeetingCreate(MeetingBase, MeetingCreateUsers):
    pass


#######

#######
# Read
#######
class UserReadWithoutMeetings(UserBase, CreatedAtRequired, IdRequired):
    pass


class MeetingReadWithoutUsers(MeetingBase, CreatedAtRequired, IdRequired):
    pass


class UserReadWithMeetings(IdRequired):
    meetings: List["MeetingUserStatusWithId"] = []


class MeetingReadWithUsers(IdRequired):
    users: List["MeetingUserStatusWithId"] = []


#######

#######
# Update
#######
class UserUpdate(UserBase):
    first_name: Optional[str] = Field(description="имя")
    second_name: Optional[str] = Field(nullable=True, description="отчество")
    last_name: Optional[str] = Field(description="фамилия")
    email: Optional[str] = Field(description="email. валидируется", index=True)


class MeetingUpdate(MeetingBase):
    name: Optional[str] = Field(description="название встречи", max_length=512)
    from_dttm: Optional[datetime] = Field(description="дата начала встречи")
    till_dttm: Optional[datetime] = Field(description="дата конца встречи")
    location: Optional[str] = Field(
        description="место встречи", nullable=True, max_length=512
    )
    description: Optional[str] = Field(
        description="описание", nullable=True, max_length=1024
    )


class MeetingUpdateUsers(MeetingCreateUsers):
    pass


class MeetingUpdateStatus(MeetingUserStatus):
    pass


#######
