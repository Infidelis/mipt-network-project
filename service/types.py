from typing import Any, Callable, Generator

from pydantic import validate_email
from sqlalchemy import Enum


class Email(str):  # pragma: no cover
    """
    Тип Email для валидоривания с помощью pydantic
    """

    @classmethod
    def __get_validators__(cls) -> Generator[Callable[[Any], str], None, None]:
        yield cls.validate

    @classmethod
    def __modify_schema__(cls, field_schema: dict[str, Any]) -> None:
        pass

    @classmethod
    def validate(cls, v: Any) -> "Email":
        if not isinstance(v, str):
            raise TypeError("string required")
        if not v:
            raise ValueError("Empty string")
        m = validate_email(v)
        if not m:
            raise ValueError(f"invalid {cls.__name__} format. Value: {v}")
        return cls(v)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({super().__repr__()})"


class RequestStatus(Enum, str):
    """Статусы заявок"""

    PENDING = "pending"
    APPROVED = "approved"
    REJECTED = "rejected"
