from fastapi import FastAPI
from fastapi.security import OAuth2PasswordBearer

from service.database import create_db_and_tables
from service.routers.auth import auth_router
from service.routers.meeting import meeting_router
from service.routers.user import user_router, user_router_with_auth


def on_startup():  # pragma: no cover
    """Метод старта сервиса"""
    create_db_and_tables()


def on_shutdown():  # pragma: no cover
    """Метод завершения работы сервиса"""
    pass


def init_app(test=False) -> FastAPI:
    """Функция инифиализации работы сервиса"""
    app = FastAPI()
    if not test:  # pragma: no cover
        app.on_event("startup")(on_startup)
        app.on_event("shutdown")(on_shutdown)

    oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
    app.include_router(auth_router)
    app.include_router(user_router_with_auth)
    app.include_router(user_router)
    app.include_router(meeting_router)
    # app.middleware("http")(log_middleware)
    return app
