# Проект по курсу: "АРХИТЕКТУРА КОМПЬЮТЕРНЫХ СЕТЕЙ"

## Описание

Сервис для планирования встреч: пользователи создают встречи,
на которых можно приглашать других участников.

Каждый пользователь имеет:
- ФИО
- Email


Про каждую встречу известно следующее:
- Название
- Дата-время начала и окончания
- Место
- Описание


Сервис предоставляет RESTful API, реализующее следующую логику:

1. создание и удаление пользователя
2. изменение данных пользователя
3. получение данных о пользователе
4. создание и удаление встречи
5. изменение встречи
6. получение деталей встречи (название, время, место и тд)
7. принятие или отмена приглашения на встречу от другого пользователя (если пользователь получил приглашение на встречу, но пока не принял или отменил её, то встреча получает статус “Под вопросом”)


Запись и чтение происходит в БД PostgreSQL

## Установка и запуск

### Локально

Если у Вас установлен python3.9

```bash
make install
make docker-pg-up
make run-server
```

### Docker


```bash
docker-compose up --build
```


## Документация

[Swagger](./docs/openapi.json)

Сервис будет доступен по ссылке: http://localhost:7777/

Тестовые запросы можно сделать из Web панели: http://0.0.0.0:7777/docs

### Регистрируем пользователя и создаем встречу

1. Создать пользователя

![1.png](./docs/imgs/1.png)
2. Нажать на кнопку `Authorize` вверху web панели

![2.png](./docs/imgs/2.png)
3. Вводим свои данные, в качесве `username` -- email. Сохраняем

![3.png](./docs/imgs/3.png)

4. Далее можем создавать встречи и регистрировать новых пользователей.
Для создания встречи нужно указывать `ID` пользователей,
которые будут на встрече.

![4.png](./docs/imgs/4.png)

5. Можем принять встречу

![5.png](./docs/imgs/5.png)
6. А после посмотреть в списке всех встреч статус

![6.png](./docs/imgs/6.png)

![7.png](./docs/imgs/7.png)
